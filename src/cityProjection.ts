import { AtomContact, WorkerAtom, AtomicRequest } from './types/atom';
import { ResourceType, ResourcePrice, ResourceStore } from './types/resource';
import Atom, { AtomData } from './components/Atom';
import { AtomicWorld } from './components/AtomicWorld';
import { createSkillRankTable } from './utils/skills';

/** 
 *  The Atomic Definition:
 *      Atoms exist by their connections to others. Many-to-many relationships abound the network of society.
 *      All points leading away from an atom are listed in the address book.
 *      All points leading toward an atom are listed in other's address books.
 *      To be listed, an Atom must perform services to fulfill needs.
 *      To fulfill needs, an Atom must have resources.
 *      Therefore, an Atom is who it knows, what it has, and what it can do.
 */

/**
 * The Atomic Mantra:
 *      To be is to be known;
 *          to be called upon, requested, and remembered.
 *      To be known is to fulfill a purpose;
 *          to provide for others when they can't continue.
 *      To provide is to have had,
 *          but to have given away.
 *      To be known,
 *          know.
 *      To be, 
 *          be.
 */

// Example world
// WORKERS
// -- Carpenter
// STORES
// -- Stockpile
// PERSONS
// -- Mono

/**
 * @param target Atom to generate contact for
 * @returns AtomContact The contact object for the atom
 */
// const getAtomContact = ({ name, atomId, location }: Atom): AtomContact => ({ name, contactCode: atomId, location, type: 'Atom' });

/**
 * @param store Store to generate contact for
 * @returns AtomContact The contact object for the store
 */
const getStoreContact = ({ name, contactId, location, canStore}: ResourceStore): AtomContact => ({ name, contactCode: contactId, location, type: 'Store', canProduce: canStore });

// A simple stockpile that can store Lumber
const stockpileStore: ResourceStore = {
    canStore: ['Lumber'], //, 'Plank'],
    contactId: 'crazy-jack-stockpile',
    name: 'Crazy Jack\'s Stockpile!',
    location: [3,1,0]
};

const atomWorld = new AtomicWorld({});

const carpenterRanks = createSkillRankTable();

carpenterRanks.Carpentry++;

// Test WorkerAtom, a carpenter that knows how to make Planks from Lumber, and knows where the Lumber store is
const carpenterAtom: WorkerAtom = {
    type: 'Carpenter',
    atom: new Atom({
        name: 'Carpenter Tim',
        atomId: 'carpenter-tim-prime',
        addressBook: {
            'Lumber': [ getStoreContact(stockpileStore) ],
        },
        location: [2,2,0],
        ranks: carpenterRanks,
    }, atomWorld),
    processes: ['Plank'],
}

// Test Atom, a single person that knows about Carpenter Tim, and he needs some Planks
const monoPersonData: AtomData = {
    name: 'Mono Syllib',
    atomId: 'mono-syllib-prime',
    addressBook: {
        'Plank': [ carpenterAtom.atom.contactInfo ],
    },
    location: [0,0,0],
};

const monoPerson = new Atom(monoPersonData, atomWorld);



// SIMULATION START
[monoPerson].forEach((i) => atomWorld.registerAtom(i));
[carpenterAtom].forEach((i) => atomWorld.registerWorker(i));
[stockpileStore].forEach((i) => atomWorld.registerStore(i));

// Seed the world for interactions
monoPerson.PLAY_GOD_allotResource("Currency", 10);
monoPerson.PLAY_GOD_engageNeed("Plank", 1);
carpenterAtom.atom.PLAY_GOD_allotResource("Plank", 1);
carpenterAtom.atom.PLAY_GOD_allotResource("Lumber", 1);

atomWorld.CommSystem.printContacts();

atomWorld.startWorld();

carpenterAtom.atom.recountLife();
monoPerson.recountLife();


// const denyRequestWithCounter = (target: AtomContact, counter: AtLeastOne<Pick<AtomicRequest, 'offered' | 'requested'>>) => {};

// const denyRequest = (target: AtomContact) => {};



/**
 * What does it look like to request a resource?
 * Requesting is: 
 * - A recursive process
 * - A potentially infinite process
 * - A Source sending a Request to a Target and receiving a Yes/No
 */
