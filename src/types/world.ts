import Atom from "components/Atom";
import { WorkerAtom, AtomContact } from "./atom";
import { ResourceStore } from "./resource";

const WORLD_ORIGIN: WorldLocation = [0,0,0];

type WorldLocation = [number, number, number];

type WorldImageState = {
    workers: WorkerAtom[];
    persons: Atom[];
    stores: ResourceStore[];
};

type WorldAddressBook = AtomContact[];

// A "Tick" is a time step in the world. Starting at 0, the world ticks up by 1 every loop representing the passage of time 
type WorldTick = number;

export {
    WorldAddressBook,
    WorldImageState,
    WorldLocation,
    WORLD_ORIGIN,
    WorldTick
}