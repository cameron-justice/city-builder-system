import { ResourceList } from "./resource";

type SkillProcess = { inputs: ResourceList, outputs: ResourceList };

type SkillRankTable = { [key in SKILL_CATEGORY]: number };

type SkillDefinitionTable = { [key in SKILL_CATEGORY]: SkillDefinition[] }

const SKILL_CATEGORIES = [
    "Carpentry",
    "MartialArts",
    "Teaching"
] as const;

type SKILL_CATEGORY = typeof SKILL_CATEGORIES[number];

// What is a skill? It's a function, f(inputs) => outputs;
// The services an atom can provide are the unique members of all its skill outputs

type SkillDefinition = {
    category: SKILL_CATEGORY,
    requiredRank: number;
    name: string;
    process: SkillProcess;
}

const CARPENTRY_SKILLS: SkillDefinition[] = [
    {
        category: "Carpentry",
        requiredRank: 1,
        name: "Classic 2x4",
        process: {
            inputs: ["Lumber"],
            outputs: ["Plank"]
        }
    },
    {
        category: "Carpentry",
        requiredRank: 2,
        name: "Batch 2x4",
        process: {
            inputs: ["Lumber", "Lumber", "Lumber"],
            outputs: ["Plank", "Plank", "Plank"]
        }
    }
]

const allSkills = [...CARPENTRY_SKILLS] as const;


export {
    allSkills,
    SkillProcess,
    SkillRankTable,
    SkillDefinition,
    SkillDefinitionTable,
    SKILL_CATEGORIES,
    SKILL_CATEGORY
}
