import Atom from "components/Atom";
import { AddressBook, AtomCommSystemMessage, AtomicMessageHandler, ContactType } from "./interactions";
import { ResourceList, ResourceType } from "./resource";
import { WorldLocation, WorldTick } from "./world";

type AtomId = string;

// An individual item
// type Atom = {
//     name: string;
//     atomId: AtomId;
//     addressBook: AddressBook;
//     memory: Memory;
//     location: WorldLocation;
//     receiveMessage: AtomicMessageHandler;
// }

type AtomContact = {
    name: string;
    contactCode: string; // Phone number, Email, blah blah blah idea
    location: WorldLocation;
    type: ContactType;
    canProduce: ResourceList;
}

// From the equation model: Source <= ({Items}<-Target) ?  {Payment}
// Source is the requesting Atom
// Items is either a single item or a list of items
// Target is the atom being requested upon
// Payment is the list of items in return for the requested item(s)
type AtomicRequest = {
    requestId: string;
    createdAt: WorldTick;
    expiresAt: WorldTick;
    source: AtomContact;
    target: AtomContact;
    requested: ResourceList; // Should these also accept a single ResourceType, not an array?
    offered: ResourceList;
}

// Types / Jobs available for worker nodes
// The idea of a "Worker Node" might be too confining for the system. Maybe all nodes have processes they can complete? Would mean that stores can be implemented as Atoms too... good idea
type WorkerAtomType = 'Carpenter' | 'Banker';

// Shallow definition of a worker node
type WorkerAtom = {
    type: WorkerAtomType;
    atom: Atom;
    processes: ResourceList;
}

export {
    AtomId,
    AtomContact,
    AtomicRequest,
    WorkerAtom,
    WorkerAtomType
}