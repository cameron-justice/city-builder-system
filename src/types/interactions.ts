import { AtomContact, AtomicRequest, AtomId } from "./atom";
import { ResourceList, ResourceType } from "./resource";

type AtomInteraction = {
    initiator: AtomId;
    target?: AtomId;
    delta: any; // The actual definition of the change that resulted from the operation
}

/**
 * The AddressBook is an atom's source of knowledge on all connections.
 * This is used to construct almost every interaction.
 * 
 * In some form, any address book must enable the following for those involved:
 *  - Contact Info
 *  - Services / Connection / Reason for being in contact book
 * 
 * Under this, a phone book is the most generic of address books.
 */
type AddressBook = {
    [key in ResourceType]?: AtomContact[];
};

type ContactType = 'Atom' | 'Store';

type AtomCommSystemMessageContent = AtomicRequest | ResourceList | string | boolean;

// TODO: document message types and their shape
const MESSAGE_TYPES = ["RESOURCE_REQUEST", "REQUEST_DENIAL", "REQUEST_FULFILLMENT", "PROCESSES_CHANGE", "PING", "DELIVERY"] as const;
type AtomCommSystemMessageType = typeof MESSAGE_TYPES[number];

type AtomCommSystemMessage = {
    senderId: string;
    receiverId: string;
    content: AtomCommSystemMessageContent;
    info?: {
        type: AtomCommSystemMessageType
    };
}

type AtomicMessageHandler = (msg: AtomCommSystemMessage) => (boolean | void);

type AtomContactRegistration = {
    contactId: string;
    deliver: AtomicMessageHandler;
}

type AtomContactRegistrationMap = Map<AtomId, AtomContactRegistration>;

type AtomicMessageDeliveryState = "FAILED" | "PROCESSING" | "SUCCESS";

type PingOptions = {
    ack?: boolean;
}

export {
    AddressBook,
    ContactType,
    AtomInteraction,
    AtomCommSystemMessage,
    AtomContactRegistration,
    AtomContactRegistrationMap,
    AtomicMessageHandler,
    PingOptions
}