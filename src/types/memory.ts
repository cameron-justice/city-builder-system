import { AtomId } from "./atom";
import { AtomInteraction } from "./interactions";

const MEMORY_TYPES = ["MESSAGE_RECEIVED", "MEETING", "REQUEST_MADE"]

type MemoryEvent = {
    atomsInvolved: AtomId[];
    interactions: AtomInteraction[];
}

export {
    MemoryEvent
}