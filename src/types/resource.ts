import { WorldLocation } from "./world";

const RESOURCE_TYPES = ['Lumber', 'Currency', 'Liquor', 'Plank'] as const;
type ResourceType = typeof RESOURCE_TYPES[number];

type ResourceList = ResourceType[];

// Prices of resources in "currency", where the 'Currency' resource is always 1 (to represent a dollar economy, no outside comparisons yet)
const ResourcePrice: { [key in ResourceType]: number} = {
    'Currency': 1,
    'Liquor': 5,
    'Lumber': 1,
    'Plank': 2
}

// Shallow definition of a store node
type ResourceStore = {
    canStore: ResourceList;
    contactId: string;
    name: string;
    location: WorldLocation;
}

type ResourceCounter = Map<ResourceType, number>;

export {
    ResourceCounter,
    ResourcePrice,
    ResourceStore,
    ResourceType,
    ResourceList,
    RESOURCE_TYPES,
}