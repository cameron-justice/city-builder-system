// Crypto
import * as crypto from "crypto";

// Lodash
import isNil from 'lodash/isNil';

// Util
import { inspect } from "util";

// App
import { AddressBook, AtomCommSystemMessage, AtomContact, AtomicRequest, AtomId, ResourceCounter, ResourceType, WorldLocation, WORLD_ORIGIN, PingOptions, RESOURCE_TYPES, ResourceList, SkillRankTable } from "../types";
import { distanceBetween, getValueOfResources, generateUniqueAtomId, compactResourceList } from "../utils";
import { AtomicWorld } from "./AtomicWorld";
import AtomCommSystem from "./CommSystem";
import { AtomMemoryStore, createBirthMemory, createMessageReceivedMemory, createResourceChangeMemory, createTradeMemory } from "./Memory";
import { createSkillRankTable, Skill } from "./Skill";

const EXPIRE_AFTER = 30;

interface IAtom {
    name: string;
    atomId?: AtomId;
    addressBook?: AddressBook;
    memory?: AtomMemoryStore;
    location?: WorldLocation;
    ranks?: SkillRankTable;
}

// Use an internal secret key system 

class Atom {
    // -- Personal Info
    private name: string;
    private atomId: AtomId;
    private location: WorldLocation;

    // -- Privacy
    private secretKey: string = crypto.generateKeySync('aes', { length: 128 }).export().toString('hex');

    // -- Connections
    private addressBook: AddressBook;

    // -- Mental
    private memory: AtomMemoryStore;
    private skill: Skill;

    // -- Greater Connections
    private atomWorld: AtomicWorld;
    private commSystem: AtomCommSystem | null = null;
    
    // Resources
    private resourcesStored: ResourceCounter = new Map(RESOURCE_TYPES.map((i) => [i, 0]));
    private resourcesNeeded: ResourceCounter = new Map(RESOURCE_TYPES.map((i) => [i, 0])); // TODO: Move this to be a PriorityQueue, add { resourceType, count, priority } items to that queue and attend to them as needed
    private resourcesAllotted: ResourceCounter = new Map(RESOURCE_TYPES.map((i) => [i, 0])); // Resources allotted but still on hand
    private pendingOutgoingRequests: AtomicRequest[] = []; // Requests sent by the atom
    private pendingIncomingRequests: AtomicRequest[] = []; // Requests sent to the atom
    private pendingFulfillments: AtomicRequest[] = []; // Requests that have been fulfilled, but trade must be initiated
    
    constructor({ 
        name,
        atomId = generateUniqueAtomId(),
        addressBook = {},
        memory = new AtomMemoryStore(),
        location = WORLD_ORIGIN,
        ranks = createSkillRankTable()
    }: IAtom, atomWorld: AtomicWorld) {
        this.name = name;
        this.atomId = atomId;
        this.addressBook = addressBook;
        this.memory = memory;
        this.location = location;

        this.atomWorld = atomWorld;

        this.commSystem = atomWorld.CommSystem;

        this.commSystem.registerContact({
            contactId: this.atomId,
            deliver: this.receiveMessage
        })

        this.skill = new Skill({
            ranks
        });

        this.memory.writeMemory(createBirthMemory(name, atomId, atomWorld.currentTick));
    }

    public perform = () => {
        console.log(`${this.name} performing...`);
        const needs = this.resourcesNeeded.entries();
        
        // Send requests to fulfill every need
        console.log(`-- Needs...`);
        Array.from(needs)
        .filter(([_, c]) => c > 0)
        .forEach(need => {
            this.fulfillNeed(...need)
        });

        // Respond to waiting requests
        console.log(`-- Incoming...`);
        this.pendingIncomingRequests.forEach(this.handleResourceRequest);

        // Receive deliveries from fulfilled requests
        console.log(`-- Fullfillments...`);
        this.pendingFulfillments.forEach(this.handleFulfilledRequest);

        console.log(`Ending ${this.name} performance...`);
    }

    private fulfillNeed = (resource: ResourceType, count: number) => {
        let contact: AtomContact | null = null;

        const storeNodes = this.stableGraph(resource);

        // for now, just take the first person that can do it
        if(storeNodes.length > 0) {
            contact = storeNodes[0][0];
        }

        if(isNil(contact)) {
            const workerNodes = this.unstableGraph(resource);

            if(workerNodes.length > 0) {
                contact = workerNodes[0][0];
            }
        }

        if(isNil(contact)) {
            return; // Lost Cause for now. Maybe a global request system or an "ask around" system would help here
        }

        const resourceList = Array(count).fill(resource);

        const request: AtomicRequest = {
            requestId: crypto.randomUUID(),
            createdAt: this.atomWorld.currentTick,
            expiresAt: this.atomWorld.currentTick + EXPIRE_AFTER,
            source: this.contactInfo,
            target: contact,
            requested: Array(count).fill(resource),
            offered: Array(getValueOfResources(resourceList)).fill("Currency")
        }

        // DECOUPLING: This call will be async, will it work the same way?
        this.commSystem?.sendMessage({
            senderId: this.atomId,
            receiverId: contact.contactCode,
            content: request,
            info: {
                type: 'RESOURCE_REQUEST'
            }
        });
        
        this.pendingOutgoingRequests.push(request);

        // Removing the need from here, presuming it will be re-added once the request is denied, or not needed if the request is fulfilled
        this.resourcesNeeded.set(
            resource,
            (this.resourcesNeeded.get(resource) ?? count) - count 
        );

        compactResourceList(request.offered).forEach(([resource, count]) => {
            this.resourcesStored.set(
                resource,
                (this.resourcesStored.get(resource) ?? count) - count
            );

            this.resourcesAllotted.set(
                resource,
                (this.resourcesAllotted.get(resource) ?? 0) + count
            )
        })
    }

    /**
     * What does a stable graph look like in data?
     * We know that it's one layer, so it could be a list of pairs...
     * Would this representation work for unstable graphs too? 
     */

    /**
     * The stable graph is the 2-layer N-ary graph of Stores that the required resource could be requested from.
     * @param resource 
     * @returns 
     */
    public stableGraph = (resource: ResourceType) => {
        const potentials = this.addressBook[resource]?.filter((contact) => contact.type === 'Store');
        
        if(!potentials) {
            // Short circuit into the unstable graph? or just return null?
            return [];
        }

        // An array to hold 
        const ranks: [AtomContact, number][] = potentials.map((i) => [i, distanceBetween(this.location, i.location)]);

        return ranks.sort((a, b) => a[1] - b[1]);

    }

    public unstableGraph = (resource: ResourceType) => {
        const potentials = this.addressBook[resource]?.filter((contact) => contact.type === 'Atom');

        if(!potentials) {
            return [];
        }

        const ranks: [AtomContact, number][] = potentials.map((i) => [i, distanceBetween(this.location, i.location)]);

        return ranks.sort((a, b) => a [1]- b[1]);
    }

    /**
     * 
     * @param resource 
     */
    public overlayGraph = (resource: ResourceType) => {
        // We need to follow the overlay spec
        // 1. Check stable graph for resource
        //     1a. If found, return that source
        // 2. Construct unstable graph composed with each node's stable graph cost
        // 3. f(n) = g(n) + h(n), where g(n) is the Node A (requesting node) unstable graph cost and h(n) is the stable graph cost of the worker node
    
        // (1)
        // get stable graph
        // (2)
        // Filter known workers by ability to create the item. 
        // Message each worker with an (INVESTIGATE_COST) type
        // -- Includes:
        //   -- Resources to find cost for
        //   -- Stream to send responses back
        //     -- { theirId, cost }
        // On every item sent through the stream, process and update the current graph costs.
        // Once every request has been answer or timed out, find the lowest cost
    };

    public receiveMessage = (msg: AtomCommSystemMessage) => {
        // Check blocked
        if(false) {
            return false;
        }

        this.memory.writeMemory(createMessageReceivedMemory(msg));

        if(isNil(msg.info)) {
            return true;
        }

        switch(msg.info?.type ?? "") {
            case "DELIVERY":
                this.updateStoredResources({ added: msg.content as ResourceList})
                break;
            case "RESOURCE_REQUEST":
                this.pendingIncomingRequests.push(msg.content as AtomicRequest);
                break;
            case "REQUEST_FULFILLMENT":
                this.receiveRequestFulfillment(msg.content as string);
                break;
            case "PING":
                this.pingContact(msg.senderId, { ack: true });
                break;
            default:
                // Message is simply a message to us. Store it in memory and acknowledge it was received
                return true;
        }
    }

    private updateStoredResources = ({ added = [], subtracted = [] }: { added?: ResourceList, subtracted?: ResourceList}) => {
        const addedCompacted = compactResourceList(added);
        const subtractedCompacted = compactResourceList(subtracted, -1);
        
        addedCompacted.forEach(([resource, count]) => {
            this.resourcesStored.set(
                resource,
                (this.resourcesStored.get(resource) ?? 0) + count,
            )
        });

        subtractedCompacted.forEach(([resource, count]) => {
            this.resourcesAllotted.set(
                resource,
                (this.resourcesAllotted.get(resource) ?? count) + count // Using (+) because the resourceList is negative
            )
        })

        this.memory.writeMemory(createResourceChangeMemory(
            this.atomId,
            [...addedCompacted, ...subtractedCompacted].map(([resource, count]) => [resource, count >= 0 ? "ADD" : "SUB", count]),
            this.atomWorld.currentTick
        ));
    }

    private receiveRequestFulfillment = (requestId: string) => {
        const requestIx = this.pendingOutgoingRequests.findIndex((i) => i.requestId === requestId);
        const [request] = this.pendingOutgoingRequests.splice(requestIx, 1);

        if(isNil(request)) {
            return;
        }

        this.pendingFulfillments.push(request);
    }

    private handleFulfilledRequest = (request: AtomicRequest) => {
        const isTradeComplete = this.commSystem?.completeTrade(request.requestId, this.atomId, request.offered);
        console.log(isTradeComplete)
        if(isTradeComplete) {
            this.memory.writeMemory(createTradeMemory(request, "REQUEST_FULFILLED", this.atomWorld.currentTick));
            this.updateStoredResources({ added: request.requested, subtracted: request.offered });
            const requestIx = this.pendingFulfillments.findIndex((i) => i.requestId === request.requestId);
            this.pendingFulfillments.splice(requestIx, 1);
        } else {
            // What do we do if it fails? When would it fail?
        }
    }

    private handleResourceRequest = (request: AtomicRequest) => {
        // for now, just check if the atom has enough of the resource
        const requestedCompact = compactResourceList(request.requested);
        const canFulfill = requestedCompact.every(([resource, count]) => (this.resourcesStored.get(resource) ?? 0) >= count);

        this.commSystem?.sendMessage({
            senderId: this.atomId,
            receiverId: request.source.contactCode,
            content: request.requestId,
            info: {
                type: canFulfill ? "REQUEST_FULFILLMENT" : "REQUEST_DENIAL",
            }
        });

        requestedCompact.forEach(([resource, count]) => {
            this.resourcesStored.set(
                resource,
                (this.resourcesStored.get(resource) ?? count) - count
            );
        });

        this.commSystem?.engageTrade(request.requestId, this.atomId, request.requested, request.offered);

        const requestIx = this.pendingIncomingRequests.findIndex((i) => request.requestId === i.requestId);

        this.pendingIncomingRequests.splice(requestIx, 1);
    }

    private pingContact = (contactId: AtomId, options: PingOptions) => {
        const { ack = false } = options;

        // this.memory.writeMemory(createPingMemory(contactId, options))

        this.commSystem?.sendMessage({
            senderId: this.atomId,
            receiverId: contactId,
            content: ack,
            info: {
                type: "PING"
            }
        });
    }
    
    public get contactInfo(): AtomContact {
        return {
            name: this.name,
            contactCode: this.atomId,
            location: this.location,
            type: 'Atom',
            canProduce: this.skill.canProduce,
        }
    };

    // Public Utilities

    public recountLife = () => {
        console.log(`\nBehold! The life of ${this.name}!\n`);
        console.log('Memories: ', inspect(this.memory.remember(), false, null));
        console.log('Riches: ', inspect(this.resourcesStored, false, null));
        console.log('Skills: ', inspect(this.skill, false, null));
        console.log('Contacts: ', inspect(this.addressBook, false, null));
    }

    // Play God

    /**
     * Adds a need for {count} of the {resource}
     * @param resource Resource to be needed
     * @param count Amount of resource to be added to needs
     */
    public PLAY_GOD_engageNeed = (resource: ResourceType, count: number) => {
        this.resourcesNeeded.set(
            resource,
            (this.resourcesNeeded.get(resource) ?? 0) + count
        );
    }

    public PLAY_GOD_allotResource = (resource: ResourceType, count: number) => {
        this.resourcesStored.set(
            resource,
            (this.resourcesStored.get(resource) ?? 0) + count
        );
    };

}

export {
    Atom,
    IAtom as AtomData
};

export default Atom;