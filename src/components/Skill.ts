import { ResourceList, ResourceType } from "types/resource";
import { allSkills, SkillDefinitionTable, SkillRankTable } from "../types/skill";
import { createSkillDefinitionTable, createSkillRankTable } from "../utils/skills";



// Generates the { SKILL_CATEGORY: T1[] } map used throughout skills
// Can't really trust this since it uses "as unknown" but leaving it because it's cool
//
// const createTable = <T1,T2>(fillValue: T1) => {
//     return Object.fromEntries(
//         SKILL_CATEGORIES.map(
//             (category: SKILL_CATEGORY) => [category, fillValue as T1]
//         )
//     ) as unknown as T2;
// }

interface ISkill {
    ranks: SkillRankTable;
}

class Skill {
    private ranks: SkillRankTable;
    private knownSkills: SkillDefinitionTable = createSkillDefinitionTable();

    constructor({
        ranks: initialRanks
    }: ISkill) {
        this.ranks = initialRanks

        this.learnAllAvailableSkills();
    }

    /**
     * Goes through the list of all skills and updates the knownSkills with every skill available to the skill rank
     */
    private learnAllAvailableSkills = () => {
        const canPerform = allSkills.filter((skill) => (
            this.ranks[skill.category] >= skill.requiredRank
        ));

        const newKnownSkills: SkillDefinitionTable = createSkillDefinitionTable();

        canPerform.forEach((skill) => newKnownSkills[skill.category].push(skill));

        this.knownSkills = newKnownSkills;
    }

    public get canProduce(): ResourceList {
        const allOutputs = Object.values(this.knownSkills)
            .flat()
            .map(({ process }) => process.outputs)
            .flat()

        return Array.from(new Set(allOutputs));
    }
}

export {
    createSkillDefinitionTable,
    createSkillRankTable,
    Skill
}