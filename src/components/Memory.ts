import { AtomCommSystemMessage, AtomicRequest, AtomId, MemoryEvent, ResourceType, WorldTick } from "types";

type MemoryType = 
    "MESSAGE_RECEIVED" 
    | "INTRODUCTION"
    | "MEETING";

const createMessageReceivedMemory = ({
    senderId,
    receiverId,
    content,
    info = {
        type: "PING"
    },
}: AtomCommSystemMessage): MemoryEvent => ({
    atomsInvolved: [senderId, receiverId],
    interactions: [{
        initiator: senderId,
        target: receiverId,
        delta: {
            type: "MESSAGE_RECEIVED",
            info: {
                senderId,
                receiverId,
                content,
                ...info
            }
        }
    }]
})

const createBirthMemory = (name: string, atomId: AtomId, tick: WorldTick): MemoryEvent => ({
    atomsInvolved: [atomId], // BORN FROM MYSELF!! AHAHAHAHHAHAHAHA
    interactions: [{
        initiator: atomId,
        target: atomId,
        delta: {
            type: "BIRTH",
            infO: {
                name,
                atomId,
                tick
            }
        }
    }]
})

type TradeStage = "REQUEST_INITIATED" | "REQUEST_CANCELLED" | "REQUEST_DENIED" | "REQUEST_FULFILLED";

const createTradeMemory = (request: AtomicRequest, tradeStage: TradeStage, tick: WorldTick): MemoryEvent => ({
    atomsInvolved: [request.source.contactCode, request.target.contactCode],
    interactions: [{
        initiator: request.target.contactCode,
        target: request.source.contactCode,
        delta: {
            type: "REQUEST_FULFILLED",
            info: {
                request,
                tick
            },
        }
    }],
});

type ResourceChangeType = "ADD" | "SUB";

type ResourceDelta = [ResourceType, ResourceChangeType, number];

const createResourceChangeMemory = (atomId: AtomId, deltas: ResourceDelta[], tick: WorldTick) => ({
    atomsInvolved: [atomId],
    interactions: deltas.map(([resource, type, count]) => ({
        initiator: atomId,
        delta: {
            type: `RESOURCE_CHANGE_${type}`,
            info: {
                resource,
                type,
                count,
                tick
            }
        }
    }))
})

class AtomMemoryStore {
    // private memories: Map<WorldTick, MemoryEvent> = new Map();
    private memories: MemoryEvent[] = [];

    constructor() {}

    /**
     * Saves a memory to the local store 
     */
    public writeMemory = (memory: MemoryEvent) => { this.memories.push(memory); }

    /**
     * 
     * @param howManyBack The number of memories to retrieve from the latest. If less than 0, retrieves all memories.
     * @returns 
     */
    public remember = (howManyBack: number = -1) => howManyBack >= 0 ? this.memories.slice(0, Math.min(this.memories.length, howManyBack)) : this.memories;
}

export {
    AtomMemoryStore,
    createBirthMemory,
    createMessageReceivedMemory,
    createResourceChangeMemory,
    createTradeMemory,
}