
// TODO: Rename interfaces with I* prefix? Drop data so it's like, IWorld or IAtom?

import { ResourceStore, WorkerAtom, WorldImageState, WorldTick } from "types";
import { inspect } from "util";
import Atom from "./Atom";
import AtomCommSystem from "./CommSystem";

interface AtomicWorldData {
    initialState?: WorldImageState;
}

class AtomicWorld {
    private commSystem = new AtomCommSystem({});

    // State
    private tick: WorldTick = 0;

    // Data Keeping
    private masterLog: [] = [];
    private initialStateRecord: WorldImageState;
    private currentStateRecord: WorldImageState;

    constructor({ 
        initialState = {
            persons: [],
            workers: [],
            stores: []
        }
    }: AtomicWorldData) {
        this.initialStateRecord = initialState;
        this.currentStateRecord = initialState;
    }

    get CommSystem() { return this.commSystem; }

    public registerAtom = (atom: Atom) => {
        this.currentStateRecord.persons.push(atom);
        console.log(`Registered ${inspect(atom)}`)
    }

    public registerWorker = (worker: WorkerAtom) => {
        this.currentStateRecord.workers.push(worker);
        console.log(`Registered ${inspect(worker)}`)
    }

    public registerStore = (store: ResourceStore) => {
        this.currentStateRecord.stores.push(store);
        console.log(`Registered ${store.name}`)
    }

    public startWorld = () => {
        
        let playing = true;

        while(this.tick < Number.MAX_SAFE_INTEGER && playing) {
            
            console.log(`Tick #${this.tick}: \n`, inspect(this.currentStateRecord))
            this.currentStateRecord.persons.forEach((person) => person.perform());
            this.currentStateRecord.workers.forEach((worker) => worker.atom.perform());

            this.tick++;

            if(this.tick > 20) playing = false;
        }
    }

    public get currentTick() { return this.tick; }
}

export {
    AtomicWorld
}