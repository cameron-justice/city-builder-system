import { isNil, cloneDeep, clone } from "lodash";
import { AtomCommSystemMessage, AtomContactRegistration, AtomContactRegistrationMap, AtomId, ResourceList, ResourceType, WorldTick } from "types";
import { compareResourceLists } from "../utils";

interface AtomCommSystemData {
    /**
     * The list of registered contacts in the communication network
     */
    contacts?: Map<AtomId, AtomContactRegistration>;
}

/** Atomic Communication System
 * -- Enrollment
 *      Enrolling in the communication system requires:
 *          1. A unique identifier (contactId)
 *          2. A method of receiving messages (deliver)
 * -- Message Passing
 *      Passing a message involves:
 *          1. Atom constructs message data
 *          2. Atom triggers communication through CommSystem (sendMessage)
 *          3. CommSystem validates the message data
 *              3a. If valid, deliver using contact's (deliver)
 *              3b. Else, inform sender of failure 
 */

class AtomCommSystem {
    private contacts: AtomContactRegistrationMap;
    private pendingTrades: Map<string, { creator: AtomId, trading: ResourceList, requiredPayment: ResourceList }> = new Map();

    constructor({ 
        contacts = new Map()
    }: AtomCommSystemData) {
        this.contacts = contacts;
    }

    // TODO: Need to have all messages be sent out between ticks, so that messages aren't sent, delivered, and handled alll in one tick

    /**
     * Handles receiving a registration request and acknowledging
     * @param contact 
     * @returns Boolean True if the contact was registered successfully
     */
    public registerContact = (contact: AtomContactRegistration) => {
        // if(!validateContactRegistration(contact)) {
        //     return false;
        // }
        this.contacts.set(contact.contactId, contact);

        return true;
    }

    public sendMessage = (msg: AtomCommSystemMessage) => {
        const { receiverId } = msg;

        // Messages require:
        // Someone to deliver to
        // Content to deliver

        const valid = this.haveContactId(receiverId);
        
        if(valid) {
            this.contacts.get(receiverId)?.deliver(msg);
            return true;
        }
    }

    public engageTrade = (tradeId: string, creator: AtomId, trading: ResourceList, requiredPayment: ResourceList = []) => {
        if(this.pendingTrades.has(tradeId)) {
            return false;
        }

        this.pendingTrades.set(tradeId, { creator, trading, requiredPayment });
    }

    public completeTrade = (tradeId: string, receiver: AtomId, payment: ResourceList = []) => {
        if(!this.pendingTrades.has(tradeId)) {
            return false;
        }

        const tradeData = cloneDeep(this.pendingTrades.get(tradeId));

        if(isNil(tradeData)) {
            return false;
        }

        if(compareResourceLists(tradeData.requiredPayment, payment)) {
            this.sendMessage({
                senderId: receiver,
                receiverId: tradeData.creator,
                content: tradeData.requiredPayment,
                info: {
                    type: "DELIVERY"
                }
            })

            this.pendingTrades.delete(tradeId);
            return true;
        }

        return false;
    }

    private haveContactId = (contactId: AtomId) => this.contacts.has(contactId);

    // Public Utilities

    public printContacts = () => console.log(this.contacts);
}

export {
    AtomCommSystem
};

export default AtomCommSystem;