import { isEqual } from "lodash";
import { ResourceType, ResourcePrice, ResourceList } from "../types";

const getValueOfResources = (resources: ResourceList) => resources.reduce((prev, curr) => prev + ResourcePrice[curr], 0);

/**
 * 
 * @param resourceList 
 * @param multiplier Value to be multiplied as transform against the resource counts. -1 produces a "negative" resource list
 * @returns 
 */
const compactResourceList = (
    resourceList: ResourceList,
    multiplier = 1
) => {
    const countMap: { [key in ResourceType]?: number } = {};
    resourceList.forEach((i) => countMap[i] = (countMap[i] ?? 0) + 1);
    return Object.entries(countMap).map(([resource, number]) => [resource, number * multiplier]) as [ResourceType, number][];
}

const countMapFromCompact = (compact: [ResourceType, number][]) => {
    const countMap: { [key in ResourceType]?: number } = {};
    compact.forEach(([resource, count]) => countMap[resource] = count);
    return countMap;
}

const countMapFromList = (list: ResourceList) => countMapFromCompact(compactResourceList(list));

const compareResourceLists = (a: ResourceList, b: ResourceList) => {
    return isEqual(
        countMapFromList(a),
        countMapFromList(b)
    );
}

export {
    compactResourceList,
    compareResourceLists,
    getValueOfResources,
}
