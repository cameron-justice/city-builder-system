import { WorldImageState, WorldLocation } from "../types/world";

// All elements using contact IDs should use this util
const generateUniqueAtomId = () => {
    const dateStr = Date
        .now()
        .toString(36); // convert num to base 36 and stringify

    const randomStr = Math
        .random()
        .toString(36)
        .substring(2, 8); // start at index 2 to skip decimal point

    return `${dateStr}-${randomStr}`;
}

const distanceBetween = (loc1: WorldLocation, loc2: WorldLocation) => Math.pow(loc2[0] - loc1[0], 2) + Math.pow(loc2[1] - loc1[1], 2) + Math.pow(loc2[2] - loc1[2], 2);

export {
    distanceBetween,
    generateUniqueAtomId
}