import { SkillDefinitionTable, SKILL_CATEGORIES, SKILL_CATEGORY, SkillDefinition, SkillRankTable } from "../types/skill";

const createSkillDefinitionTable: () => SkillDefinitionTable = () => {
    return Object.fromEntries(
        SKILL_CATEGORIES.map(
            (category: SKILL_CATEGORY) => [category, [] as SkillDefinition[]])
        ) as SkillDefinitionTable;
}

const createSkillRankTable: () => SkillRankTable = () => {
    return Object.fromEntries(
        SKILL_CATEGORIES.map(
            (category: SKILL_CATEGORY) => [category, 0])
        ) as SkillRankTable;
}

export {
    createSkillDefinitionTable,
    createSkillRankTable
}
